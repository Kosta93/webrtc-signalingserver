// useful libs

const https = require('http');
const WebSocket = require('websocket').server;
const fs = require('fs');

// general variables

const port = process.env.PORT || 8444;
const webRtcClients = [];
const webRtcDiscussions = {};

const privateKey = fs.readFileSync('./cert/key.pem').toString();
const certificate = fs.readFileSync('./cert/cert.pem').toString();

// web server functions

const httpsServer = https.createServer((req, res) => {
    res.write('Hello.');
    res.end();
});

httpsServer.listen(port, () => {
    console.log('Server is listening (port ' + port + ')"');
});

// web socket functions

const webSocketServer = new WebSocket({httpServer: httpsServer});

webSocketServer.on('request', request => {
    console.log('New request (' + request.origin + ')');

    const connection = request.accept(null, request.origin);
    console.log('New connection (' + connection.remoteAddress + ')');
    webRtcClients.push(connection);
    connection.id = webRtcClients.length - 1;

    connection.on('message', message => {
        if (message.type === 'utf8') {
            console.log('Got message ' + message.utf8Data + '');
            let signal;
            try {
                signal = JSON.parse(message.utf8Data);
                if (signal) {
                    if (signal.token !== undefined) {
                        if (webRtcDiscussions[signal.token] === undefined) {
                            webRtcDiscussions[signal.token] = {};
                            webRtcDiscussions[signal.token][connection.id] = true;
                            roomStatusNotification();
                            return;

                        }
                        if (Object.keys(webRtcDiscussions[signal.token]).length < 2) {
                            webRtcDiscussions[signal.token][connection.id] = true;
                        }

                        try {
                            Object.keys(webRtcDiscussions[signal.token]).forEach(id => {
                                if (id != connection.id) {
                                    // console.log(webRtcDiscussions[signal.token]);
                                    console.log('Sending message to: ' + id);
                                    webRtcClients[+id].send(Buffer.from(message.utf8Data).toString('base64'), err => console.error('Sending error: ' + err));
                                }
                            });
                        } catch (e) {
                            console.error(e)
                        }
                    } else {
                        console.error(`Invalid signal ${message.utf8Data}`);
                    }
                    if (signal.type === 'callee_leaved') {
                        console.log('Callee leaved..');
                        removeParticipantFromDiscussion(connection.id);
                    }
                    roomStatusNotification();
                } else {
                    console.error(`Invalid signal ${message.utf8Data}`);
                }
            } catch (e) {
                console.error('Message can not be parsed.. ' + message.utf8Data + ' ');
            }
        }
    });

    connection.on('close', () => {
        console.log('Connection closed: ' + connection.id);
        removeParticipantFromDiscussion(connection.id);
    });
});

// utility functions

function removeParticipantFromDiscussion(connectionId) {
    Object.keys(webRtcDiscussions).forEach(token => {
        Object.keys(webRtcDiscussions[token]).forEach(id => {
            if (id == connectionId) {
                delete webRtcDiscussions[token][id];
            }
        });
    })
}

function roomStatusNotification() {
    const rooms = [];
    Object.keys(webRtcDiscussions).forEach(room => {
        rooms.push({roomName: room, numParticipants: Object.keys(webRtcDiscussions[room]).length, maxParticipants: 2});
    });
    webRtcClients.forEach(connection => {
        connection.send(Buffer.from(JSON.stringify(Object.assign({type: 'rooms'}, rooms))).toString('base64'),
            err => console.error('Send discussions error: ' + err));
    })
}

function logError(error) {
    if (error !== 'Connection closed' && error !== undefined) {
        logComment('ERROR: ' + ' ' + error);
    }
}

function logComment(comment) {
    console.log(`${new Date()} ${comment}`);
}
